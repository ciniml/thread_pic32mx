/**
 * @file thread.h
 * @brief Concurrent thread library for PIC32MX
 * @auther Kenta IDA
 * @date 2013/07/02
 */
#ifndef	__THREAD_H__
#define	__THREAD_H__

#ifndef	__ASSEMBLER__

typedef	struct tag_ThreadInfo
{
	struct tag_ThreadInfo*	prev;
	struct tag_ThreadInfo*	next;
	void*		stack;
} ThreadInfo;
typedef	void (*ThreadProc)(void*);

/**
 * @brief �Creates a new thread and appends the thread to the thread list.
 * @param[in] info  Specifies a pointer to ThreadInfo struct which holds the thead information to create.
 * @param[in] proc  A thread entry point
 * @param[in] param Parameter passed to the thread entry point.
 */
extern void threadCreate(ThreadInfo* info, ThreadProc proc, void* param);
/**
 * @brief Destroies the specified thread.
 * @param[in] thread A pointer to the ThreadInfo struct of thread to destroy.
 */
extern void threadDestroy(ThreadInfo* thread);
/**
 * @brief Yields current thread and executes next thread.
 */
extern void threadYield(void);
/**
 * @brief Retrieves the address of the current thread's TIB.
 */
extern ThreadInfo* threadCurrent(void);

extern void threadRun(void);

#endif	//__ASSEMBLER__

#endif	//__THREAD_H__
